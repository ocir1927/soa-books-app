const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const request = require('request');

const app = express();
const port = 3000;

// Where we will keep books
let books = [];

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/books', (req, res) => {
    request({
        uri: 'https://www.googleapis.com/books/v1/volumes',
        qs: {
          q: req.query['query'],
          startIndex: req.query['startIndex']
        }
      }).pipe(res);

});

// getBooks(query, startIndex): Observable<Book[]> {
//   return this.http.get<Book[]>(`${environment.googleBooksBaseApi}?q=${query}&startIndex=${startIndex}`)
//     .pipe(
//       map(resp => {
//         let books = resp['items'];
//         return books.map(book => {
//           let newBook = new Book();
//           newBook.title = book['volumeInfo']['title'];
//           newBook.authors = book['volumeInfo']['authors'];
//           newBook.description = book['volumeInfo']['description'];
//           newBook.publishedDate = book['volumeInfo']['publishedDate'];
//           newBook.pageCount = book['volumeInfo']['pageCount'];
//           // newBook.textSnippet = book['searchInfo']['textSnippet'];
//           newBook.smallThumbnail = book['volumeInfo']['imageLinks']['smallThumbnail'];
//           newBook.thumbnail = book['volumeInfo']['imageLinks']['thumbnail'];
//           return newBook;
//         })
//       }),
//     )
// }

app.listen(port, () => console.log(`Book service app works on http://localhost:${port} !`));