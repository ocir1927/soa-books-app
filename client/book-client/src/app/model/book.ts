export class Book{
    title: string;
    authors: string[];
    publishedDate: Date;
    description: string;
    pageCount: number;
    textSnippet: string;
    smallThumbnail: string;
    thumbnail: string;;
}