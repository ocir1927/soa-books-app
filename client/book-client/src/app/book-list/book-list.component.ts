import { Component, OnInit } from '@angular/core';
import { BooksService } from './books.service';
import { Book } from '../model/book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  books = [];
  crtIndex: number = 1;
  searchQuery: string = 'flower';  

  constructor(private bookService: BooksService) { }

  ngOnInit() {
    this.getBooks();
  }

  getBooks(){
    this.bookService.getBooks(this.searchQuery,this.crtIndex).subscribe((books:Book[]) => {
      this.books = books;
      console.log(this.books)
    })
  }

}
