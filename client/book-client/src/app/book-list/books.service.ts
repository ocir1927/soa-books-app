import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators/'
import { Book } from '../model/book';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {



  constructor(private http: HttpClient) {
  }

  getBooks(query, startIndex): Observable<Book[]> {
    return this.http.get<Book[]>(`${environment.baseApi}/books?query=${query}&startIndex=${startIndex}`)
      .pipe(
        map(resp => {
          let books = resp['items'];
          return books.map(book => {
            let newBook = new Book();
            newBook.title = book['volumeInfo']['title'];
            newBook.authors = book['volumeInfo']['authors'];
            newBook.description = book['volumeInfo']['description'];
            newBook.publishedDate = book['volumeInfo']['publishedDate'];
            newBook.pageCount = book['volumeInfo']['pageCount'];
            // newBook.textSnippet = book['searchInfo']['textSnippet'];
            newBook.smallThumbnail = book['volumeInfo']['imageLinks']['smallThumbnail'];
            newBook.thumbnail = book['volumeInfo']['imageLinks']['thumbnail'];
            return newBook;
          })
        }),
      )
  }


}
